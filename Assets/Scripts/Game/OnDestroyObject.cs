﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroyObject : MonoBehaviour
{

    EnemySpawner spawner;

    private void Awake()
    {
        spawner = GameObject.Find("Start").GetComponent<EnemySpawner>();
    }

    private void OnDestroy()
    {
        switch (spawner._NowWave)
        {
            case 1:
                spawner.CurrentEnemiesDestroyWave_1++;
                break;
            case 2:
                spawner.CurrentEnemiesDestroyWave_2++;
                break;
            case 3:
                spawner.CurrentEnemiesDestroyWave_3++;
                break;
            default:
                break;

        }

    }
}
