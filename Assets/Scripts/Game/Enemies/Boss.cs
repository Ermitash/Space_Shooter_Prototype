﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : SpaceShip
{
    public Slider SliderObject;
    public GameObject Explosion;
    public GameObject Explosion_2;
    public int Score;

    Weapon[] _Weapons;
    Vector3[] _WeaponsPosition;
    Vector3 _EndPosition;

    float _leftBorder;
    float _rightBorder;
    float _fullHealth;

    bool DirectionMove;
    bool DirectionForward;
    
    private void Start()
    {
        _EndPosition = new Vector3(0, 4.0f, 0);
        DirectionForward = true;
        _WeaponsPosition = new Vector3[10];
        _Weapons = new Weapon[10];
        _fullHealth = health;
        _leftBorder = -5.29f;
        _rightBorder = 5.29f;
        DirectionMove = false;

        SliderObject.gameObject.SetActive(true);
    }

    public override void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary" || other.gameObject.tag == "Defender" || other.gameObject.tag == "HealthBonus" || other.gameObject.tag == "Bonus")
            return;

        health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;     
        SliderObject.value = health / _fullHealth;


        if (health <= 0)
        {
            SliderObject.gameObject.SetActive(false);
            ScoreText.UpdateScore(Score);
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Instantiate(Explosion_2, transform.position+Vector3.right*3, Quaternion.identity);
            Instantiate(Explosion_2, transform.position-Vector3.right * 3, Quaternion.identity);
            GameObject.Find("Start").gameObject.GetComponent<Finish>().enabled = true;
            Destroy(gameObject.transform.parent.gameObject);
        }
    }


    private void FixedUpdate()
    {
        MoveShip();

        if (!DirectionForward)
        {
            InitializationWeaponPositions();
            for (int i = 0; i < _Weapons.Length; i++)
            {
                _Weapons[i].Position = _WeaponsPosition[i];
                _Weapons[i].AutoShoot();
            }
        }
    }

    private void MoveShip()
    {
        if (DirectionForward)
        {
            if(Vector3.Distance(Sheath.transform.position,_EndPosition)<0.01)
            {
                DirectionForward = false;
                InitializationWeaponPositions();
                InitializeWeapons();
            }
            else
            Sheath.transform.position -= Vector3.up * Time.deltaTime;
        }
        else
        {
            if (Sheath.transform.position.x <= _leftBorder)
                DirectionMove = true;
            else if (Sheath.transform.position.x >= _rightBorder)
                DirectionMove = false;

            if (DirectionMove == false)
                Sheath.transform.position -= Vector3.right * SpeedShipMovement * Time.deltaTime;
            else
                Sheath.transform.position += Vector3.right * SpeedShipMovement * Time.deltaTime;
        }
    }

    void InitializationWeaponPositions()
    {
        Vector3 transform = Sheath.transform.position;
        _WeaponsPosition[0] = new Vector3(transform.x - 3.373f, transform.y - 0.46f, transform.z);
        _WeaponsPosition[1] = new Vector3(transform.x - 2.324f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[2] = new Vector3(transform.x - 1.637f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[3] = new Vector3(transform.x - 1.207f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[4] = new Vector3(transform.x - 0.358f, transform.y - 2.8f, transform.z);
        _WeaponsPosition[5] = new Vector3(transform.x + 0.361f, transform.y - 2.8f, transform.z);
        _WeaponsPosition[6] = new Vector3(transform.x + 1.207f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[7] = new Vector3(transform.x + 1.645f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[8] = new Vector3(transform.x + 2.335f, transform.y - 0.9f, transform.z);
        _WeaponsPosition[9] = new Vector3(transform.x + 3.377f, transform.y - 0.46f, transform.z);
    }

    void InitializeWeapons()
    {
        Quaternion NewRotation = Sheath.transform.rotation;
        NewRotation.z = -180.0f;

        _Weapons[0] = new Weapon(_WeaponsPosition[0], NewRotation, 1.5f, 0);
        _Weapons[1] = new Weapon(_WeaponsPosition[1], NewRotation, 2.2f, 0);
        _Weapons[2] = new Weapon(_WeaponsPosition[2], NewRotation, 3.3f, 1);
        _Weapons[3] = new Weapon(_WeaponsPosition[3], NewRotation, 1.8f, 0);
        _Weapons[4] = new Weapon(_WeaponsPosition[4], NewRotation, 1f, 0);
        _Weapons[5] = new Weapon(_WeaponsPosition[5], NewRotation, 1f, 0);
        _Weapons[6] = new Weapon(_WeaponsPosition[6], NewRotation, 1.8f, 0);
        _Weapons[7] = new Weapon(_WeaponsPosition[7], NewRotation, 3.3f, 1);
        _Weapons[8] = new Weapon(_WeaponsPosition[8], NewRotation, 2.2f, 0);
        _Weapons[9] = new Weapon(_WeaponsPosition[9], NewRotation, 1.5f, 0);
    }
}
