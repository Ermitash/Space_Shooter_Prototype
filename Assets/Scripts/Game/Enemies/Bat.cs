﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : ObjectSpaceShooter
{

    public GameObject Explosion;
    public int Score;
    public override void OnPause(bool arg)
    {
        if (arg)
        {
            transform.parent.GetComponent<BezierSolution.BezierWalkerWithSpeed>().enabled = false;
            transform.parent.GetComponent<Animator>().enabled = false;
        }
        else
        {
            transform.parent.GetComponent<BezierSolution.BezierWalkerWithSpeed>().enabled = true;
            transform.parent.GetComponent<Animator>().enabled = true;
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary" || other.gameObject.tag == "Bat" || other.gameObject.tag == "HealthBonus" || other.gameObject.tag == "Bonus")
            return;

        health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;


        if (health <= 0)
        {
            ScoreText.UpdateScore(Score);
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject.transform.parent.gameObject);
        }

    }
}
