﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyShip : SpaceShip
{
    public Animator Enemy;
    public GameObject Explosion;
    public int Score;
    private Weapon _Weapon;
    float _leftBorder;
    float _rightBorder;
    float x;
    float AccuracyBetweenTwoPoints;

    // Use this for initialization
    void Start()
    {
        AccuracyBetweenTwoPoints = 0.1f;
        _leftBorder = -8.3f;
        _rightBorder = 8.3f;

        x = Random.Range(_leftBorder, _rightBorder);
        while(true)
        {
            if (Mathf.Abs(x - transform.position.x) > _rightBorder)
                x = Random.Range(_leftBorder, _rightBorder);
            else
                break;
        }
        Vector3 NewWeaponPosition = transform.position + transform.forward;
        _Weapon = new Weapon(NewWeaponPosition, new Quaternion(0, 0, 180, 1), 2, 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        AutoShipMovement();
        Vector3 NewWeaponPosition = transform.position + transform.forward;
        _Weapon.Position = NewWeaponPosition;
        
    }

    public void AutoShipMovement()
    {
        Sheath.transform.position -= Vector3.up * SpeedShipMovement * Time.deltaTime;


        if (Sheath.transform.position.x > x && Mathf.Abs(Sheath.transform.position.x - x) > AccuracyBetweenTwoPoints)
        {
            Sheath.transform.position -= Vector3.right * SpeedShipMovement * Time.deltaTime;
            if (Enemy.GetFloat("Rotate") != 1)
                Enemy.SetFloat("Rotate", 1);
        }
        else if (Sheath.transform.position.x < x && Mathf.Abs(Sheath.transform.position.x - x) > AccuracyBetweenTwoPoints)
        {
            Sheath.transform.position += Vector3.right * SpeedShipMovement * Time.deltaTime;
            if (Enemy.GetFloat("Rotate") != -1)
                Enemy.SetFloat("Rotate", -1);
        }
        else
        {
            Enemy.SetFloat("Rotate", 0);
            _Weapon.AutoShoot();
        }
    }

    public override void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary" || other.gameObject.tag == "HealthBonus" || other.gameObject.tag == "Bonus")
            return;

        health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;

        if (health <= 0)
        {
            ScoreText.UpdateScore(Score);
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject.transform.parent.gameObject);
        }
    }


}
