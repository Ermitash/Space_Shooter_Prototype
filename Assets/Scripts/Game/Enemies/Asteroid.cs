﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Asteroid : ObjectSpaceShooter
{
    public float speed;
    Vector3 RotateAsteroid;

    public GameObject Explosion;
    public int Score;

    // Use this for initialization
    private void Start()
    {
        RotateAsteroid = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));

        while (RotateAsteroid.x == 0 && RotateAsteroid.y == 0 && RotateAsteroid.z == 0)
            RotateAsteroid = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));

        GetComponent<Rigidbody>().angularVelocity = RotateAsteroid;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position -= Vector3.up * speed * Time.deltaTime;
    }

    public override void OnPause(bool arg)
    {
        if (arg)
        {
            GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
            enabled = false;
        }
        else
        {
            GetComponent<Rigidbody>().angularVelocity = RotateAsteroid;
            enabled = true;
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary" || other.gameObject.tag == "HealthBonus" || other.gameObject.tag == "Bonus")
            return;

        
        health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;
        
        if (health <= 0)
        {
            ScoreText.UpdateScore(Score);
            GameObject explosion = Instantiate(Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
