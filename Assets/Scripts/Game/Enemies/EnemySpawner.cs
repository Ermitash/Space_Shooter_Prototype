﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{

    public GameObject Asteroid;
    public GameObject Ship;
    public GameObject Bat;
    public GameObject Boss;
    public Slider HP;
    public AudioClip bossClip;
    public AudioSource MusicSource;
    public BezierSolution.BezierSpline Route_1;
    public BezierSolution.BezierSpline Route_2;
    public GameObject TextWaitTime;



    [SerializeField]
    float StartWait;
    [SerializeField]
    float WaitBetweenWaves;
    [SerializeField]
    float WaitBetweenEnemies;

    float _leftBorder;
    float _rightBorder;

    float timer;
    bool StartWaitBool;
    bool WaitBool;
    bool Pause;
    bool SpawnBoss;

    public int _NowWave { get; private set; }

    private void Awake()
    {
        WaitBool = false;
        _NowWave = 1;
        SpawnBoss = false;
        EventPause.PauseEvent += OnPause;

        _leftBorder = -8.3f;
        _rightBorder = 8.3f;

        timer = 0;
        StartWaitBool = true;
        Pause = false;
    }

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Wave_1", StartWait, WaitBetweenEnemies);
        TextWaitTime.GetComponent<WaveTimer>().WaitTime = StartWait;
        TextWaitTime.GetComponent<WaveTimer>().NowWave = _NowWave;
    }

    private void OnPause(bool arg)
    {
        if (arg)
        {
            CancelInvoke();
            Pause = true;
        }
        else
        {
            if (StartWaitBool == true)
                InvokeRepeating("Wave_1", StartWait - timer, WaitBetweenEnemies);
            else if(WaitBool)
                switch (_NowWave)
                {
                    case 1:
                        InvokeRepeating("Wave_1", WaitBetweenWaves - timer, WaitBetweenEnemies);
                        break;
                    case 2:
                        InvokeRepeating("Wave_2", WaitBetweenWaves - timer, WaitBetweenEnemies);
                        break;
                    case 3:
                        InvokeRepeating("Wave_3", WaitBetweenWaves - timer, WaitBetweenEnemies);
                        break;
                    case 4:
                        if (!SpawnBoss)
                        {
                            Invoke("Wave_4", WaitBetweenWaves - timer);
                            break;
                        }
                        else
                            break;

                    default: break;
                }
            else
                switch (_NowWave)
                {
                    case 1:
                        InvokeRepeating("Wave_1", WaitBetweenEnemies - timer, WaitBetweenEnemies);
                        break;
                    case 2:
                        InvokeRepeating("Wave_2", WaitBetweenEnemies - timer, WaitBetweenEnemies);
                        break;
                    case 3:
                        InvokeRepeating("Wave_3", WaitBetweenEnemies - timer, WaitBetweenEnemies);
                        break;
                    case 4:
                        if (!SpawnBoss)
                        {
                            Invoke("Wave_4", WaitBetweenWaves - timer);
                            break;
                        }
                        else
                            break;

                    default: break;
                }

            Pause = false;
        }
    }

    private void FixedUpdate()
    {
        if (Pause == false)
            timer += Time.deltaTime;
    }



    [SerializeField]
    int CountEnemiesWave_1;
    int CurrentEnemiesWave_1 = 0;
    public int CurrentEnemiesDestroyWave_1 { get; set; }

    void Wave_1()
    {
        if ((CurrentEnemiesWave_1 > CountEnemiesWave_1 - 1) && (CurrentEnemiesDestroyWave_1 == CountEnemiesWave_1))
        {
            timer = 0;
            CancelInvoke();
            WaitBool = true;
            TextWaitTime.gameObject.SetActive(true);
            TextWaitTime.GetComponent<WaveTimer>().WaitTime = WaitBetweenWaves;
            InvokeRepeating("Wave_2", WaitBetweenWaves, WaitBetweenEnemies);
            _NowWave++;
            TextWaitTime.GetComponent<WaveTimer>().NowWave = _NowWave;
        }
        else if (CurrentEnemiesWave_1 <= CountEnemiesWave_1 - 1)
        {
            if (StartWaitBool == true)
                StartWaitBool = false;

            Vector3 SpawnPosition = new Vector3(Random.Range(_leftBorder, _rightBorder), 6, 0);
            Instantiate(Asteroid, SpawnPosition, Quaternion.identity);
            CurrentEnemiesWave_1++;
            timer = 0;
        }

    }


    [SerializeField]
    int CountEnemiesWave_2;
    int CurrentEnemiesWave_2 = 0;
    public int CurrentEnemiesDestroyWave_2 { get; set; }
    void Wave_2()
    {
        if ((CurrentEnemiesWave_2 > CountEnemiesWave_2 - 1) && (CurrentEnemiesDestroyWave_2 == CountEnemiesWave_2))
        {
            timer = 0;
            CancelInvoke();
            WaitBool = true;
            TextWaitTime.gameObject.SetActive(true);
            TextWaitTime.GetComponent<WaveTimer>().WaitTime = WaitBetweenWaves;
            InvokeRepeating("Wave_3", WaitBetweenWaves, WaitBetweenEnemies);
            _NowWave++;
            TextWaitTime.GetComponent<WaveTimer>().NowWave = _NowWave;
        }
        else if(CurrentEnemiesWave_2 <= CountEnemiesWave_2 - 1)
        {
            WaitBool = false;

            Vector3 SpawnPosition = new Vector3(Random.Range(_leftBorder, _rightBorder), 6, 0);

            int RandomElements = Random.Range(0, 2);

            if (RandomElements == 0)
                Instantiate(Asteroid, SpawnPosition, Quaternion.identity);
            else
            {
                GameObject EnemyShip = Instantiate(Ship, SpawnPosition, Quaternion.identity);
                EnemyShip.transform.Rotate(new Vector3(90, 180, 0));
            }

            CurrentEnemiesWave_2++;
            timer = 0;
        }
    }



    [SerializeField]
    int CountEnemiesWave_3;
    int CurrentEnemiesWave_3 = 0;
    public int CurrentEnemiesDestroyWave_3 { get; set; }
    void Wave_3()
    {
        if ((CurrentEnemiesWave_3 > CountEnemiesWave_3-1) && (CurrentEnemiesDestroyWave_3 == CountEnemiesWave_3*2))
        {
            timer = 0;
            CancelInvoke();
            WaitBool = true;
            TextWaitTime.gameObject.SetActive(true);
            TextWaitTime.GetComponent<WaveTimer>().WaitTime = WaitBetweenWaves;
            Invoke("Wave_4", WaitBetweenWaves);
            _NowWave++;
        }
        else if(CurrentEnemiesWave_3 <= CountEnemiesWave_3 - 1)
        {
            Vector3 SpawnPosition = new Vector3(0, 6, 0);
            WaitBool = false;
            GameObject BatEnemy = Instantiate(Bat, SpawnPosition, Quaternion.identity);
            BatEnemy.GetComponent<BezierSolution.BezierWalkerWithSpeed>().spline = Route_1;

            BatEnemy = Instantiate(Bat, SpawnPosition, Quaternion.identity);
            BatEnemy.GetComponent<BezierSolution.BezierWalkerWithSpeed>().spline = Route_2;

            CurrentEnemiesWave_3++;
            timer = 0;
        }
    }



    void Wave_4()
    {
        SpawnBoss = true;
        MusicSource.clip = bossClip;
        MusicSource.Play();
        GameObject bossObject = Instantiate(Boss, new Vector3(0f, 7.83f, 0f), Quaternion.identity);
        bossObject.transform.FindChild("BossModel").GetComponent<Boss>().SliderObject = HP;
    }
}
