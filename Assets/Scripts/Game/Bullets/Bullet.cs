﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : ObjectSpaceShooter
{

    [SerializeField]
    public float speed;

    void MovingBullet()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }

    public override void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;

    }

    private void FixedUpdate()
    {
        MovingBullet();
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary" || other.gameObject.tag == "HealthBonus" || other.gameObject.tag == "Bonus")
            return;

        health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;

        if (health <= 0)
            Destroy(gameObject);
    }
}
