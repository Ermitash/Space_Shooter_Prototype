﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyItself : MonoBehaviour {

    public float TimeToDestroy;
    float timer = 0;
	
	// Update is called once per frame
	void Update () {
        if (timer > TimeToDestroy)
            Destroy(this.gameObject);
        else
            timer += Time.deltaTime;
	}
}
