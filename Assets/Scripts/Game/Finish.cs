﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public GameObject Defender;
    public GameObject FinishMenu;

    public AudioSource MusicPlayer;
    public AudioClip Win;

    float TimeUntilMove;
    float TimeUntilAppearFinish;
    public float speed;
    float timer;

    bool MusicPlay;
    // Use this for initialization
    void Start()
    {
        MusicPlay = false;
        TimeUntilAppearFinish = 6f;
        TimeUntilMove = 3f;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (timer > TimeUntilMove)
        {
            if (MusicPlay == false)
            {
                MusicPlayer.clip = Win;
                MusicPlayer.Play();
                MusicPlay = true;
            }

            if (Defender)
            {
                Defender.transform.FindChild("StarSparrow2").GetComponent<DefenderShip>().enabled = false;
                Defender.transform.position += Vector3.up * speed * Time.deltaTime;
            }
        }

        if(timer>TimeUntilAppearFinish)
        {
            FinishMenu.SetActive(true);
        }

        timer += Time.deltaTime;
    }
}
