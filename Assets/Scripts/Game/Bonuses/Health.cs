﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : ObjectSpaceShooter {

    public float speed;
    AudioSource bonusPlayer;
    public AudioClip bonus;

	// Use this for initialization
	void Start () {
        bonusPlayer = GameObject.Find("Audio").transform.FindChild("SoundPlayer").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.parent.position -= Vector3.up * speed*Time.deltaTime;
	}

    public override void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Defender")
        {
            bonusPlayer.clip = bonus;
            bonusPlayer.Play();
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
