﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour {

    public GameObject Health;
    public GameObject RocketBonus;
    public GameObject LazerBonus;

    public float TimeSpawn;
    float time;

    float _leftBorder;
    float _rightBorder;
    float _UpBorder;
    float _DownBorder;

    Vector3 SpawnPosition;
    int ViewBonus;

	// Use this for initialization
	void Start () {
        EventPause.PauseEvent += OnPause;

        time = 0;
        _leftBorder = -8.3f;
        _rightBorder = 8.3f;
        _UpBorder = 4.5f;
        _DownBorder = -1f;
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

        if(time>TimeSpawn)
        {
            time = 0;
            SpawnPosition = new Vector3(Random.Range(_leftBorder, _rightBorder), Random.Range(_UpBorder, _DownBorder), 0);
            ViewBonus = Random.Range(0, 3);

            switch (ViewBonus)
            {
                case 0:
                    Instantiate(Health, SpawnPosition, Quaternion.identity);
                    break;
                case 1:
                    Instantiate(RocketBonus, SpawnPosition, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(LazerBonus, SpawnPosition, Quaternion.identity);
                    break;
                default:
                    break;
            }
        }
	}

    void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;
    }
}
