﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenderShip : SpaceShip
{

    public GameObject DefenderModel;                         //модель корабля
    protected enum TypeOfShooting { SmallBullet, Rocket };   //виды стрельбы

    public int _CurrentBullet;                             //текущий вид патронов  
    private Weapon[] _Weapons;                              //кол оружий
    private Vector3[] _WeaponsPosition;                     //позиции оружий

    public GameObject Explosion;
    public GameObject LoseMenu;
    public int _CountWeapons;

    public GameObject MusicSource;
    public AudioClip LoseClip;

    public Text HealthText;

    public int CountWeapons
    {
        get
        {
            return _CountWeapons;
        }
        set
        {
            if (_CurrentBullet == 0)
            {
                if (_CountWeapons < 6)
                    _CountWeapons = value;
            }
            else if (_CurrentBullet == 1)
            {
                if (_CountWeapons < 4)
                    _CountWeapons = value;
            }
        }
    }

    // Use this for initialization
    void Start()
    {

        HealthText.text = "Health: " + health.ToString();

        _WeaponsPosition = new Vector3[5];

        InitializePositionWeapons();

        _CurrentBullet = (int)TypeOfShooting.SmallBullet;

        _Weapons = new Weapon[5];

        for (int i = 0; i < _Weapons.Length; i++)
            _Weapons[i] = new Weapon(_WeaponsPosition[i], Quaternion.identity, 0.15f, _CurrentBullet);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveTheShip();
        InitializePositionWeapons();

        foreach (Weapon i in _Weapons)
        {
            i._CurrentBullet = _CurrentBullet;
        }

        for (int i = 0; i < CountWeapons; i++)            //расположение начала пуль, у маленьких пуль 5 точек расположения, у больших пуль их 3
        {
            if (_CurrentBullet == 1)
            {
                if (i == 0)
                {
                    _Weapons[i].Position = _WeaponsPosition[i];
                    _Weapons[i].Shoot();
                }
                else
                {
                    _Weapons[i].Position = _WeaponsPosition[i + 2];
                    _Weapons[i].Shoot();
                }
            }
            else
            {
                _Weapons[i].Position = _WeaponsPosition[i];
                _Weapons[i].Shoot();
            }

        }

    }

    void InitializePositionWeapons()
    {
        Vector3 transform = DefenderModel.transform.position;

        if (_CurrentBullet == 0)
        {
            _WeaponsPosition[0] = new Vector3(transform.x, transform.y + 1.25f, transform.z);
            _WeaponsPosition[1] = new Vector3(transform.x + 0.212f, transform.y + 1.1f, transform.z);
            _WeaponsPosition[2] = new Vector3(transform.x - 0.212f, transform.y + 1.1f, transform.z);
            _WeaponsPosition[3] = new Vector3(transform.x + 0.596f, transform.y + 0.7f, transform.z);
            _WeaponsPosition[4] = new Vector3(transform.x - 0.596f, transform.y + 0.7f, transform.z);
        }
        else
        {
            _WeaponsPosition[0] = new Vector3(transform.x, transform.y + 1.5f, transform.z);
            _WeaponsPosition[3] = new Vector3(transform.x + 0.596f, transform.y + 1f, transform.z);
            _WeaponsPosition[4] = new Vector3(transform.x - 0.596f, transform.y + 1f, transform.z);
        }
    }

    public override void OnPause(bool pause)
    {
        if (pause)
        {
            GetComponent<DefenderShip>().enabled = false;
            GetComponent<DefenderAnimator>().enabled = false;
            DefenderModel.GetComponent<Animator>().enabled = false;
            DefenderModel.transform.Find("Particle System").GetComponent<ParticleSystem>().Pause();
        }
        else
        {
            GetComponent<DefenderShip>().enabled = true;
            GetComponent<DefenderAnimator>().enabled = true;
            DefenderModel.GetComponent<Animator>().enabled = true;
            DefenderModel.transform.Find("Particle System").GetComponent<ParticleSystem>().Play();
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boundary")
            return;

        if (other.gameObject.tag == "HealthBonus")
            health += other.gameObject.GetComponent<ObjectSpaceShooter>().damage;
        else if(other.gameObject.tag == "Bonus")
        { }
        else
            health -= other.gameObject.GetComponent<ObjectSpaceShooter>().damage;

        HealthText.text = "Health: " + health.ToString();

        if (health <= 0)
        {
            MusicSource.GetComponent<AudioSource>().clip = LoseClip;
            MusicSource.GetComponent<AudioSource>().loop = false;
            MusicSource.GetComponent<AudioSource>().Play();
            EventPause.PauseEvent -= MusicSource.GetComponent<MusicSettings>().OnPause;
            EventPause.LaunchingPauseEvents(true);

            LoseMenu.SetActive(true);
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
