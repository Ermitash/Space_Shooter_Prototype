﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderAnimator : MonoBehaviour {

    public Animator Defender;

    public float x;
	// Update is called once per frame
	void FixedUpdate () {
        x = Input.GetAxis("Horizontal");
        Defender.SetFloat("Horizontal",x);
	}
}
