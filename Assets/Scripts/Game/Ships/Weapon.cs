﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public float SpeedShooting { get; set; }
    public float SpeedShootingRocket { get; set; }
    private float _LastTimeOfShooting;
    public int _CurrentBullet { get; set; }

    public Vector3 Position { get; set; }
    public Quaternion Rotation { get; set; }

    public Weapon(Vector3 position, Quaternion rotation, float SpeedShooting, int CurrentBullet)
    {
        SpeedShootingRocket = 0.3f;
        this.SpeedShooting = SpeedShooting;
        Position = position;
        Rotation = rotation;
        _LastTimeOfShooting = Time.time;
        _CurrentBullet = CurrentBullet;

    }

    public void Shoot()
    {
        if (Input.GetAxis("Fire1") == 1)
        {

            if (_CurrentBullet == 0)
            {
                if ((Time.time - _LastTimeOfShooting) > SpeedShooting)
                {
                    GameObject NewBullet = Instantiate(Resources.Load("Models/Bullets/lazer/SmallBullet") as GameObject, Position, Rotation);
                    _LastTimeOfShooting = Time.time;
                }
            }
            else if (_CurrentBullet == 1)
            {
                if ((Time.time - _LastTimeOfShooting) > SpeedShootingRocket)
                {
                    GameObject NewBullet = Instantiate(Resources.Load("Models/Bullets/Rocket/RocketBullet") as GameObject, Position, Rotation);
                    _LastTimeOfShooting = Time.time;
                }
            }
        }
    }

    public void AutoShoot()
    {
        if ((Time.time - _LastTimeOfShooting) > SpeedShooting)
        {
            RaycastHit hit;
            if (Physics.Raycast(Position, -Vector3.up, out hit, 100f))
            {
                if (hit.transform.tag != "Defender")
                {
                    return;
                }
            }

            if (_CurrentBullet == 0)
            {
                GameObject NewBullet = Instantiate(Resources.Load("Models/Bullets/lazer/SmallBullet") as GameObject, Position, Rotation);
                _LastTimeOfShooting = Time.time;
            }
            else if (_CurrentBullet == 1)
            {
                GameObject NewBullet = Instantiate(Resources.Load("Models/Bullets/Rocket/RocketBullet") as GameObject, Position, Rotation);
                _LastTimeOfShooting = Time.time;
            }
        }
    }
}
