﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpaceShip : ObjectSpaceShooter {

    [SerializeField]
    protected float SpeedShipMovement;
    public GameObject Sheath;

    public void MoveTheShip(float maxUp = 8f, float maxDown = -0.3f, float maxLeft = -8.07f, float maxRight = 8.07f)
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        float NewPositionX =(x * SpeedShipMovement) * Time.deltaTime;
        float NewPositionY = (y * SpeedShipMovement) * Time.deltaTime;

        if (Sheath.transform.position.x+ NewPositionX < maxRight && Sheath.transform.position.x + NewPositionX > maxLeft)
            Sheath.transform.position += new Vector3(NewPositionX,0,0);

        if(Sheath.transform.position.y + NewPositionY < maxUp && Sheath.transform.position.y + NewPositionY > maxDown)
            Sheath.transform.position += new Vector3(0, NewPositionY, 0);
    }

   

}
