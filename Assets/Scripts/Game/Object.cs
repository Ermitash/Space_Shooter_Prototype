﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectSpaceShooter : MonoBehaviour {

    public int health;
    public int damage;
    public abstract void OnPause(bool arg);
    public abstract void OnTriggerEnter(Collider other);
    private void Awake()
    {
        EventPause.PauseEvent += OnPause;
    }

    protected void OnDestroy()
    {
        EventPause.PauseEvent -= OnPause;
    }

}
