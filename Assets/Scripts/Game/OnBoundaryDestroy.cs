﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBoundaryDestroy : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "EnemyShip" || other.gameObject.tag == "Defender" || other.gameObject.tag == "Bat")
        {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
        else
            Destroy(other.gameObject);
    }
}
