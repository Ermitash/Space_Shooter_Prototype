﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScoreCompleteText : MonoBehaviour {

    Text TextScore;
    int i;
    float timer;
    float delay;
    // Use this for initialization
    void Start () {
        delay = 0.0001f;
        timer = 0;
        i = 0;
        TextScore = GetComponent<Text>();
        

    }

    private void Update()
    {
        if(timer>delay && i<ScoreText.score)
        {
            i++;
            TextScore.text = "You score: " + i.ToString();
            timer = 0;
        }
        

        timer += Time.deltaTime;
    }
}
