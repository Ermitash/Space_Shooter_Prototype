﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxScore : MonoBehaviour {

    public static int MaxPoints;
    Text TextMaxScore;
    // Use this for initialization
    void Start () {
        TextMaxScore = GetComponent<Text>();
        MaxPoints = PlayerPrefs.GetInt("MaxScore");
	}
	
	// Update is called once per frame
	void Update () {
        TextMaxScore.text = "Max score: " + MaxPoints.ToString();
    }

    public static void UpdateMaxScore(int arg)
    {
        MaxPoints = arg;
        PlayerPrefs.SetInt("MaxScore", arg);

    }
}
