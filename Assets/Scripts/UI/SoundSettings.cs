﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSettings : MonoBehaviour
{

    AudioSource _sound;
    // Use this for initialization
    void Start()
    {
        _sound = GetComponent<AudioSource>();
        _sound.volume = PlayerPrefs.GetFloat("sound_volume", 0.5f);

        EventAudio.SoundEvent += ChangeVolume;
        EventPause.PauseEvent += OnPause;
    }

    public void ChangeVolume(float value)
    {
        if (_sound != null)
            _sound.volume = value;
    }

    public void OnPause(bool arg)
    {
        if (arg)
            _sound.Pause();
        else
            _sound.UnPause();
    }

    private void OnDestroy()
    {
        EventPause.PauseEvent -= OnPause;
    }
}
