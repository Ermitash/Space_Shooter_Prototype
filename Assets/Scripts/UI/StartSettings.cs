﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StartSettings : MonoBehaviour
{

    public Dropdown ListResolutionsDropdown;
    public Toggle FullScreenToggle;
    public Slider MusicSlider;
    public Slider SoundSlider;

    public AudioClip pressed;
    public AudioSource current;

    public ButtonIsActive currentButton;
    public Button Apply;

    Resolution[] resolutions;



    // Use this for initialization
    private void OnEnable()    
    {
        resolutions = Screen.resolutions;
        List<string> options = new List<string>();

        ListResolutionsDropdown.ClearOptions();
        for (int i = 0; i < resolutions.Length; i++)
            options.Add(resolutions[i].ToString());
        
        ListResolutionsDropdown.AddOptions(options);

        for (int i = 0; i < resolutions.Length; i++)
        {
            if (CompareScreenParametrs(resolutions[i]))
            {
                ListResolutionsDropdown.value = i;
                break;
            }
        }

        bool _fullScreen = (PlayerPrefs.GetInt("full_screen") == 1) ? true : false;
        FullScreenToggle.isOn = _fullScreen;

        MusicSlider.value = PlayerPrefs.GetFloat("music_volume", 0.5f);
        SoundSlider.value = PlayerPrefs.GetFloat("sound_volume", 0.5f);

        Apply.interactable = false;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && gameObject.activeSelf == true)
        {
            if (pressed != null && !(current.isPlaying))
            {
                current.clip = pressed;
                current.Play();
            }

            currentButton.isDisabled();
            gameObject.SetActive(false);
        }
    }

    bool CompareScreenParametrs(Resolution one)
    {
        if (one.width == Screen.width && one.height == Screen.height && one.refreshRate == Screen.currentResolution.refreshRate)
            return true;
        else
            return false;
    }
}