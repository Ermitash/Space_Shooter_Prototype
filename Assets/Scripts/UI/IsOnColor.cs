﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsOnColor : MonoBehaviour {


   public void ChangeColor()
    {
        Toggle current = GetComponent<Toggle>();
        ColorBlock colors= current.colors;

        if (current.isOn == true)
            colors.normalColor = colors.highlightedColor;
        else
            colors.normalColor = Color.white;

        current.colors = colors;
    }
}
