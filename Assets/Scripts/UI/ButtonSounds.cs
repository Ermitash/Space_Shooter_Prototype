﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSounds : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler {

    public AudioClip highlighted;
    public AudioClip pressed;
    public AudioSource current;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (highlighted != null && !(current.isPlaying))
        {
            current.clip = highlighted;
            current.Play();
        }
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (pressed != null && !(current.isPlaying))
        {
            current.clip = pressed;
            current.Play();
        }
    }


}
