﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class InMainMenu : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(MainMenu);
    }

    void MainMenu()
    {
        EventPause.DeleteAllReferences();
        SceneManager.LoadSceneAsync("Start");
    }
}
