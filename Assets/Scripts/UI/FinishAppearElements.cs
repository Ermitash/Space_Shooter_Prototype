﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishAppearElements : MonoBehaviour {

    public GameObject TextComplete;
    public GameObject TextScoreComplete;
    public GameObject Button;

    public GameObject[] Stars;

    float TimeForAppearTextComplete;
    float timer;
	// Use this for initialization
	void Start () {
        EventPause.LaunchingPauseEvents(true);
        TimeForAppearTextComplete = 1f;
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (timer > TimeForAppearTextComplete)
            TextComplete.SetActive(true);

        if (timer > 1.25f)
            Stars[0].SetActive(true);

        if (timer > 1.5f && ScoreText.score>600)
            Stars[1].SetActive(true);

        if (timer > 1.75f && ScoreText.score > 800)
            Stars[2].SetActive(true);

        if (timer > 2f)
        {
            TextScoreComplete.SetActive(true);
        }

        if (timer > 2.25f)
        {
            Button.SetActive(true);
        }

        timer += Time.deltaTime;
	}
}
