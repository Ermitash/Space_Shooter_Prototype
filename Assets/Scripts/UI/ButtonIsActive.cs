﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIsActive : MonoBehaviour {

    public GameObject[] buttons;
    Color disabled;

    // Use this for initialization
    public void isActive ()
    {
        ColorBlock colors = GetComponent<Button>().colors;
        disabled = colors.disabledColor;
        colors.disabledColor = colors.highlightedColor;
        GetComponent<Button>().colors = colors;

        foreach (GameObject i in buttons)
        {
            i.GetComponent<Button>().interactable = false;
            i.GetComponent<ButtonSounds>().enabled = false;
        }
    }

    public void isDisabled()
    {
        ColorBlock colors = GetComponent<Button>().colors;
        colors.disabledColor = disabled;
        GetComponent<Button>().colors = colors;

        foreach (GameObject i in buttons)
        {
            i.GetComponent<Button>().interactable = true;
            i.GetComponent<ButtonSounds>().enabled = true;
        }
    }
}
