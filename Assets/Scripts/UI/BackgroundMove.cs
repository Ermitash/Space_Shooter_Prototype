﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundMove : MonoBehaviour {

    public float speed;

    Vector3 BeginPosition;
    Vector3 EndPosition;

	// Use this for initialization
	void Start () {
        EventPause.PauseEvent += OnPause;
        BeginPosition = new Vector3(-2.267044f, 8.61f, 0);
        EndPosition = new Vector3(-2.267044f, -12.44f, 0);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        

        if (Vector3.Distance(transform.localPosition, EndPosition) < 0.2f)
            transform.localPosition = BeginPosition;
        else
            transform.localPosition -= Vector3.up * speed * Time.deltaTime;


    }

    void OnPause(bool arg)
    {
        if (arg)
            enabled = false;
        else
            enabled = true;
    }
}
