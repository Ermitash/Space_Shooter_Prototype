﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NotificationSounds : MonoBehaviour {

    public AudioSource NotificationPlayer;
    public AudioClip Notification;

    private void OnEnable()
    {
        NotificationPlayer.clip = Notification;
        NotificationPlayer.Play();
    }
}
