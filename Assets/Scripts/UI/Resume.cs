﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resume : MonoBehaviour {

    public GameObject QuickMenuObject;

    float time;
    float deltaTime;

    private void Start()
    {
        deltaTime = 0.1f;
        time = Time.time;
        GetComponent<Button>().onClick.AddListener(Continue);
    }

    void Continue()
    {
        if (QuickMenuObject.activeSelf == false && Time.time - time > deltaTime)
        {
            EventPause.LaunchingPauseEvents(true);
            QuickMenuObject.SetActive(true);
            time = Time.time + deltaTime;
        }
        else if (QuickMenuObject.activeSelf == true && Time.time - time > deltaTime)
        {
            EventPause.LaunchingPauseEvents(false);
            QuickMenuObject.SetActive(false);
            time = Time.time + deltaTime;
        }


    }
}
