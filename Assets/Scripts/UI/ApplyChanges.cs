﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class ApplyChanges : MonoBehaviour
{
    

    public Dropdown ListResolutionsDropdown;
    public Toggle FullScreenToggle;
    public Slider MusicSlider;
    public Slider SoundSlider;


    public GameParametrs CurrentGameParametrs;
    CurrentParametrs newParametrs;

    int _width;
    int _height;
    bool _fullScreen;
    int _refreshRate;

    public void ApplyNewParametrs()
    {
        getResolution(out _width, out _height, out _refreshRate);

        if (FullScreenToggle.isOn)
            _fullScreen = true;
        else
            _fullScreen = false;
        
        newParametrs = new CurrentParametrs();
        newParametrs.setParametrs(_width, _height, _fullScreen, _refreshRate, MusicSlider.value, SoundSlider.value);
        newParametrs.ApplyParametrs();
    }

    public void SaveChanges()
    {
        CurrentGameParametrs.nowParametrs=newParametrs;
    }

    public void CancelNewParametrs()
    {
        CurrentGameParametrs.nowParametrs.ApplyParametrs();

        FullScreenToggle.isOn = CurrentGameParametrs.nowParametrs.fullScreen;
        MusicSlider.value = CurrentGameParametrs.nowParametrs.musicVolume;
        SoundSlider.value = CurrentGameParametrs.nowParametrs.soundVolume;

        List<Dropdown.OptionData> a = ListResolutionsDropdown.options;
        string currentRes = CurrentGameParametrs.nowParametrs.width.ToString() + " x " + CurrentGameParametrs.nowParametrs.height.ToString() + " @ " + CurrentGameParametrs.nowParametrs.refreshRate.ToString()+"Hz";

        for (int i=0;i<a.Count;i++)
        {
            if (currentRes == a[i].text)
                ListResolutionsDropdown.value = i;
        }

       
    }

    void getResolution(out int width, out int height, out int refreshRate)
    {
        string selected = ListResolutionsDropdown.captionText.text;
        Resolution[] resolutions = Screen.resolutions;

        Resolution WillBeTheCurrentResolution = new Resolution();
        for (int i = 0; i < resolutions.Length; i++)
        {
            if (selected == resolutions[i].ToString())
            {
                WillBeTheCurrentResolution = resolutions[i];
                break;
            }
        }

        width = WillBeTheCurrentResolution.width;
        height = WillBeTheCurrentResolution.height;
        refreshRate = WillBeTheCurrentResolution.refreshRate;

    }

    public void ApplyButtonActive()
    {
        GetComponent<Button>().interactable = true;
    }

}

