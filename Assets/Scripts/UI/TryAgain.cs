﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TryAgain : MonoBehaviour {

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(NewGame);
    }

    void NewGame()
    {
        EventPause.DeleteAllReferences();
        SceneManager.LoadSceneAsync("Game_level");
    }
}
