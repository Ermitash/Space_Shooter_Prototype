﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetMaxScore : MonoBehaviour
{
    public void ResetScore()
    {
        MaxScore.UpdateMaxScore(0);
    }
}