﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventAudio : MonoBehaviour {

    public static event Action<float> SoundEvent;
    public static event Action<float> MusicEvent;

    public static void LaunchingSoundEvents(float arg) { SoundEvent(arg); }
    public static void LaunchingMusicEvents(float arg) { MusicEvent(arg); }
}
