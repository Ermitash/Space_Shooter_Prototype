﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventPause : MonoBehaviour {

    public delegate void Pause(bool arg);
    public static event Pause PauseEvent;

    public static void LaunchingPauseEvents(bool arg) { PauseEvent(arg); }
    public static void DeleteAllReferences()
    {
        PauseEvent = null;
    }
}
