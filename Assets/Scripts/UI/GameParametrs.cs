﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct CurrentParametrs
{
    public int width;
    public int height;
    public bool fullScreen;
    public int refreshRate;
    public float musicVolume;
    public float soundVolume;

    public void setParametrs(int width, int height, bool fullScreen, int refreshRate, float musicVolume, float soundVolume)
    {
        this.width = width;
        this.height = height;
        this.fullScreen = fullScreen;
        this.refreshRate = refreshRate;
        this.musicVolume = musicVolume;
        this.soundVolume = soundVolume;
    }

    public void ApplyParametrs()
    {
        Screen.SetResolution(width, height, fullScreen, refreshRate);

        if (fullScreen == true)
            PlayerPrefs.SetInt("full_screen", 1);
        else
            PlayerPrefs.SetInt("full_screen", 0);

        PlayerPrefs.SetInt("width_screen", width);
        PlayerPrefs.SetInt("height_screen", height);
        PlayerPrefs.SetFloat("music_volume", musicVolume);
        PlayerPrefs.SetFloat("sound_volume", soundVolume);
        EventAudio.LaunchingMusicEvents(musicVolume);
        EventAudio.LaunchingSoundEvents(soundVolume);
    }
}


public class GameParametrs : MonoBehaviour {

    public CurrentParametrs nowParametrs;

    // Use this for initialization
    void Start () {
        nowParametrs = new CurrentParametrs();
        nowParametrs.setParametrs(Screen.currentResolution.width, Screen.currentResolution.height, Screen.fullScreen, Screen.currentResolution.refreshRate, PlayerPrefs.GetFloat("music_volume", 0.5f), PlayerPrefs.GetFloat("sound_volume", 0.5f));
    }

}
