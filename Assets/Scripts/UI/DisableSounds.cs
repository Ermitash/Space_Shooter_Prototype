﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableSounds : MonoBehaviour
{

    public Slider sound;
    public Slider music;
    Toggle CurrentToggle;
    bool inZero;
    // Use this for initialization
    void Start()
    {
        CurrentToggle = GetComponent<Toggle>();

        sound.onValueChanged.AddListener(changeSlider);
        music.onValueChanged.AddListener(changeSlider);

        CurrentToggle.onValueChanged.AddListener(ChangeToggle);
        inZero = false;
    }

    private void changeSlider(float arg0)
    {
        if (inZero == false)
        {
            if (music.value == 0 && sound.value == 0)
            {
                CurrentToggle.isOn = true;
                CurrentToggle.enabled = false;
                inZero = true;
            }

        }
        else
        {
            if (music.value != 0 || sound.value != 0)
            {
                CurrentToggle.enabled = true;
                CurrentToggle.isOn = false;
                inZero = false;
            }

        }
    }

    void ChangeToggle(bool change_toggle)
    {    

        if (change_toggle == true)
        {
            sound.value = 0;
            music.value = 0;
        }

    }
}