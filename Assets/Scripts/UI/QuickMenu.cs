﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickMenu : MonoBehaviour {

    public GameObject QuickMenuObject;

    float time;
    float deltaTime;

    private void Start()
    {
        deltaTime = 0.1f;
        time = Time.time;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetAxis("Cancel") == 1 && QuickMenuObject.activeSelf==false && Time.time-time>deltaTime)
        {
            EventPause.LaunchingPauseEvents(true);
            QuickMenuObject.SetActive(true);
            time = Time.time + deltaTime;
        }          
        else if (Input.GetAxis("Cancel") == 1 && QuickMenuObject.activeSelf == true && Time.time - time > deltaTime)
        {
            EventPause.LaunchingPauseEvents(false);
            QuickMenuObject.SetActive(false);
            time = Time.time + deltaTime;
        }
            

    }
}
