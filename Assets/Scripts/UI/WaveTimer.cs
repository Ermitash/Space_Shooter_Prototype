﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WaveTimer : MonoBehaviour
{


    public float WaitTime { get; set; }
    public int NowWave { get; set; }
    float timer;

    bool Pause;

    private void Awake()
    {
        timer = 0;
    }

    // Use this for initialization
    void Start()
    {
        Pause = false;
        EventPause.PauseEvent += OnPause;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Pause)
        {
            if (WaitTime > 0)
            {
                timer += Time.deltaTime;

                if (NowWave == 1 && WaitTime <= 3)
                    GetComponent<Text>().text = "Get ready";
                else if(NowWave != 1)
                    GetComponent<Text>().text = "The next wave after: " + WaitTime.ToString();

                if (timer >= 1)
                {
                    WaitTime--;
                    timer = 0;
                }
            }
            else
            {
                if (NowWave == 1 && timer < 3)
                {
                    GetComponent<Text>().text = "Forward!";
                    timer += Time.deltaTime;
                }
                else if (gameObject.active)
                {
                    gameObject.active = false;
                    timer = 0;
                }
            }
        }
    }

    private void OnPause(bool arg)
    {
        if (arg)
            Pause = true;
        else
            Pause = false;
    }
}
