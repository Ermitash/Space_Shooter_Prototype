﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour {
 
    public static int score;
    Text TextScore;

    // Use this for initialization
    void Start()
    {
        TextScore = GetComponent<Text>();
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (score > MaxScore.MaxPoints)
            MaxScore.UpdateMaxScore(score);

        TextScore.text = "Score: " + score.ToString();
    }

    public static void UpdateScore(int arg )
    {
        score += arg;
    }

}
