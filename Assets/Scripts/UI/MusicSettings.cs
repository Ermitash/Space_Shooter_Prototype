﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSettings : MonoBehaviour
{

    AudioSource _music;
    // Use this for initialization
    void Start()
    {
        _music = GetComponent<AudioSource>();
        _music.volume = PlayerPrefs.GetFloat("music_volume", 0.5f);

        EventAudio.MusicEvent += ChangeVolume;
        EventPause.PauseEvent += OnPause;
    }

    public void ChangeVolume(float value)
    {
        if (_music != null)
            _music.volume = value;
    }

    public void OnPause(bool arg)
    {
        if (arg)
            _music.Pause();
        else
            _music.UnPause();
    }

    private void OnDestroy()
    {
        EventPause.PauseEvent -= OnPause;
    }
}